### Heart Disease - UCI dataset

- [notebook preview](https://micstan.gitlab.io/share_zone/uci_heart.html)
- Classic dataset for predicting heart disease from 14 features (age, gender, chol etc.)
- https://www.kaggle.com/ronitf/heart-disease-uci
- https://archive.ics.uci.edu/ml/datasets/Heart+Disease
